import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import Vuex from 'vuex'
import axios from 'axios'
import EventHub from 'vue-event-hub'

Vue.use(Vuex)
Vue.use(EventHub)

import VuexStore from './store'
const store = new Vuex.Store(VuexStore)
let {state} = VuexStore.modules.system

state.$api = axios.create({
  //baseURL: 'https://api.bigoferta.com/'
  baseURL: 'https://jsonplaceholder.typicode.com/'
})


Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
