import system from './system'
import posts from './posts'
export default {
    modules: {
        system, posts
    }
}