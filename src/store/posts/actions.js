export default {
    async getPosts({getters,commit}, {start, limit})
    {
        let {data} = await getters.getApi.get(`posts?_start=${start}&_limit=${limit}`)
        commit('SET_POSTS', data)
    },

    async savePost({getters}, post)
    {
        let {data} = await getters.getApi.post('posts', post)
        console.log(data)
        return data
    },

    async atualizaPost({getters}, post)
    {
        let {data} = await getters.getApi.put(`posts/${post.id}`, post)
        console.log(data)
        return data
    },

    async deletaPost({getters}, id)
    {
        let {data} = await getters.getApi.delete(`posts/${id}`)
        console.log(data)
        return data
    }
}